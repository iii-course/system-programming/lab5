CC=gcc
CFLAGS=-Wall -Werror -g -ggdb
LIBS=-lrt
SRC=src
SERVER_O=daemon.o files.o formatting.o logging.o socket.o server.o
CLIENT_O=formatting.o logging.o socket.o client.o

main: 
	$(CC) $(CFLAGS) main.c -o main

clean:
	rm -rf *.o main
