#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/select.h>
#include <unistd.h>

struct Layer3Block {
    char header[40];
    char data[60];
};

struct Layer2Block {
    uint8_t STX;
    uint8_t COUNT[3];
    uint8_t ACK;
    uint8_t SEQ;
    uint8_t LFRAME;
    uint8_t RESVD;
    
    char header[40];
    char data[60];

    uint8_t CHKSUM[2];
    uint8_t ETX;
};

int MAX_SEGMENT_SIZE = 60;
int BUFFER_NUM_OF_PDU = 40;
int TTR = 16;

void transmit4(const char *filename);
void transmit3(char *buffer, size_t buffer_size);
void transmit2(struct Layer3Block blocks[], size_t number_of_blocks);
int transmit1(struct Layer2Block *package);

void receive4(const char *filename);
char *receive3();
struct Layer3Block *receive2();
struct Layer2Block *receive1();

int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Required arguments are not provided");
        exit(EXIT_FAILURE);
    }

    if (strcmp(argv[1], "-t") == 0) {
        transmit4(argv[2]);
    } else if (strcmp(argv[1], "-r") == 0) {
        receive4(argv[2]);
    } else {
        printf("Unknown option: %s", argv[1]);
        exit(EXIT_FAILURE);
    }
}

void transmit4(const char *filename) {
    FILE *file = fopen(filename, "r");

    if (file == NULL) {
        exit(EXIT_FAILURE);
    }

    // https://www.fundza.com/c4serious/fileIO_reading_all/index.html

    fseek(file, 0L, SEEK_END);
    long numbytes = ftell(file);

    fseek(file, 0L, SEEK_SET);	
    
    char *buffer = (char*)calloc(numbytes, sizeof(char));	
    if (buffer == NULL) {
        exit(EXIT_FAILURE);
    }

    fread(buffer, sizeof(char), numbytes, file);
    transmit3(buffer, numbytes);

    free(buffer);
    fclose(file);
}

void transmit3(char *buffer, size_t buffer_size) {
    int number_of_blocks = buffer_size / MAX_SEGMENT_SIZE;
    struct Layer3Block *blocks = (struct Layer3Block *)calloc(BUFFER_NUM_OF_PDU, sizeof(struct Layer3Block));

    for (int i = 0; i < number_of_blocks && i < BUFFER_NUM_OF_PDU; i++) {
        int number_of_bytes_to_copy = MAX_SEGMENT_SIZE;

        if (i == number_of_blocks - 1) {
            number_of_bytes_to_copy = buffer_size - MAX_SEGMENT_SIZE * (i - 1);
        }

        memcpy(&blocks[i].data, &buffer[i * number_of_blocks], number_of_bytes_to_copy);
    }

    transmit2(blocks, number_of_blocks);
}

void transmit2(struct Layer3Block blocks[], size_t number_of_blocks) {
    struct Layer2Block *new_blocks = (struct Layer2Block *)blocks;

    for (int i = 0; i < number_of_blocks; i ++) {
        struct Layer2Block *new_block = &new_blocks[i];

        new_block->STX = 0x02;
        new_block->COUNT[0] = 0;
        new_block->COUNT[1] = 0;
        new_block->COUNT[2] = sizeof(blocks[i].data);
        new_block->ACK = 0;
        new_block->SEQ = i;
        new_block->LFRAME = i == number_of_blocks - 1 ? 0 : 0x01;
        new_block->RESVD = 0;

        new_block->CHKSUM[0] = 0;
        new_block->CHKSUM[1] = sizeof(struct Layer2Block);
        new_block->ETX = 0x03;

        for (int count = 0; count < TTR; count++) {
            if (transmit1(new_block) == 0) {
                break;
            }
        }
    }
}

int transmit1(struct Layer2Block *package) {
    bool should_write = true;
    const char *pipename = "pipe";

    for (int count = 0; count < TTR && should_write; count++) {
        int fd = open(pipename, 0644, O_CREAT | O_RDWR);
        if (fd < 0) {    
            exit(EXIT_FAILURE);
        }

        size_t bytes_written_total = 0;
        size_t package_size = sizeof(package);

        while (bytes_written_total < package_size) {
            fd_set set;
            FD_ZERO(&set);
            FD_SET(fd, &set);

            if (select(fd + 1, NULL, &set, NULL, NULL) != 0) {
                int bytes_written = write(
                    fd, package + bytes_written_total, package_size - bytes_written_total
                );
                if (bytes_written == -1) {
                    should_write = true;
                    break;
                } 
            }
        }

        should_write = false;
        return 0;
    }
    
    return -1;
}

void receive4(const char *filename) {
    bool is_first = true;

    char *buffer = NULL;
    while ((buffer = receive3()) != NULL) {
        char *rules = "a";

        if (is_first) {
            is_first = false;
            rules = "w";
        }

        FILE *file = fopen(filename, rules);
        if (file == NULL) {
            exit(EXIT_FAILURE);
        }

        fputs(buffer, file);

        fclose(file);
        free(buffer);    
    }
}

char *receive3() {
    struct Layer3Block *received_blocks = receive2();
    size_t number_of_blocks = BUFFER_NUM_OF_PDU;

    char *buffer = (char *)calloc(MAX_SEGMENT_SIZE * number_of_blocks, sizeof(char));
    if (buffer == NULL) { 
        return buffer;
    }

    for (size_t i = 0; i < number_of_blocks; i++) {
        memcpy(&buffer[i * MAX_SEGMENT_SIZE], &received_blocks[i].data, MAX_SEGMENT_SIZE);
    }

    return buffer;
}

struct Layer3Block *receive2() {
    struct Layer3Block *received_blocks = (struct Layer3Block *)receive1();
    return received_blocks;
}

struct Layer2Block *receive1() {
    const char *pipename = "pipe";

    int fd = open(pipename, 0644, O_RDONLY);
    if (fd < 0) {    
        exit(EXIT_FAILURE);
    }

    size_t bytes_to_read = sizeof(struct Layer2Block);
    struct Layer2Block *packages = (struct Layer2Block *)calloc(
        BUFFER_NUM_OF_PDU, sizeof(struct Layer2Block)
    );

    for (int i = 0; i < BUFFER_NUM_OF_PDU; i++) {
        fd_set set;
        FD_ZERO(&set);
        FD_SET(fd, &set);

        if (select(fd + 1, &set, NULL, NULL, NULL) != 0) {
            if (read(fd, &packages[i], bytes_to_read) < 0) {
                exit(EXIT_FAILURE);
            }

            packages[i].SEQ = i;
            packages[i].ACK = 0x06;
        }
    }

    return packages;
}